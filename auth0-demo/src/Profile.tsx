import { useAuth0 } from "@auth0/auth0-react";
import { Link } from "react-router-dom";
import LogoutButton from "./LogoutButton";

const Profile = () => {
  const { user, isAuthenticated, isLoading } = useAuth0();

  if (isLoading) {
    return <div>Loading ...</div>;
  }

  return (
    <div>
      {isAuthenticated ? (
        <div>
          <h1>Hello {user?.given_name}!</h1>
          <img src={user ? user.picture : ""} alt={user ? user.name : ""} />
          <p>
            <b>Email:</b>
            {user ? user.email : ""}
          </p>
          <p>
            <b>Felhasználónév:</b>
            {user ? user.nickname : ""}
          </p>
          <p>
            <b>Teljes név:</b>
            {user ? user.name : ""}
          </p>
          <Link className="btn btn-primary" to="/roletest">
            Role tests
          </Link>
          <LogoutButton />
        </div>
      ) : (
        <div>Unauthenticated</div>
      )}
    </div>
  );
};

export default Profile;
function useHistory() {
  throw new Error("Function not implemented.");
}
