import "./App.css";
import LoginButton from "./LoginButton";
import Profile from "./Profile";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import RoleTest from "./RoleTest";

function App() {
  return (
    <BrowserRouter>
      <div className="card">
        <Routes>
          <Route path="/profile" element={<Profile />} />
          <Route path="/roletest" element={<RoleTest />} />
          <Route path="/" element={<LoginButton />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
