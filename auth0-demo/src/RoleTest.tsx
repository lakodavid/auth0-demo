import { useAuth0 } from "@auth0/auth0-react";
import { Link } from "react-router-dom";
import LogoutButton from "./LogoutButton";

const RoleTest = () => {
  const { user, isAuthenticated, isLoading } = useAuth0();

  const { getAccessTokenSilently } = useAuth0();

  console.log(isAuthenticated);
  if (isLoading) {
    return <div>Loading ...</div>;
  }

  const callPublicEndpoint = async () => {
    try {
      const response = await fetch("http://localhost:3001/public");
      const responseData = await response.json();
      console.log(responseData);
    } catch (error) {
      console.log(error);
    }
  };
  const callProtectedEndpoint = async () => {
    try {
      const token = await getAccessTokenSilently();
      const response = await fetch("http://localhost:3001/protected", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      const responseData = await response.json();
      console.log(responseData);
    } catch (error) {
      console.log(error);
    }
  };

  const callRoleBasedEndpoint = async () => {
    try {
      const token = await getAccessTokenSilently();
      const response = await fetch("http://localhost:3001/role", {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <h1>Üdv a role-oknál</h1>
      <p>Válasz egyet és tesztelheted is az api lábakat eléred-e</p>
      <button className="btn btn-primary" onClick={callPublicEndpoint}>
        Publikus láb
      </button>
      <button className="btn btn-primary" onClick={callProtectedEndpoint}>
        Privát láb (csak autentikált user)
      </button>
      <button className="btn btn-primary" onClick={callRoleBasedEndpoint}>
        Privát láb (jogosultság kell hozzá)
      </button>
      <Link className="btn btn-primary" to="/profile">
        Vissza a profil oldalra
      </Link>
    </div>
  );
};

export default RoleTest;
